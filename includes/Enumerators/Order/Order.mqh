//+------------------------------------------------------------------+
//|                                      Enumerators/Order/Order.mqh |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| See LICENSE file for License                                     |
//| See README.md file for usage                                     |
//|                                                                  |
//| Provide Constants and Enuerators for Order Placement             |
//+------------------------------------------------------------------+

// Custom enum for the standard order types that can be placed
// MQL4 has ENUM_ORDER_TYPE which includes all the values below, but
// has extra ones as inherited from MQL5, but unusable in MQL4
enum ENUM_ORDER_TYPE_ALL {
   Buy         = OP_BUY,
   Sell        = OP_SELL,
   BuyStop     = OP_BUYSTOP,
   BuyLimit    = OP_BUYLIMIT,
   SellStop    = OP_SELLSTOP,
   SellLimit   = OP_SELLLIMIT,
};

enum ENUM_ORDER_TYPE_PENDING {
   PendingBuyStop     = OP_BUYSTOP,
   PendingBuyLimit    = OP_BUYLIMIT,
   PendingSellStop    = OP_SELLSTOP,
   PendingSellLimit   = OP_SELLLIMIT,
};

enum ENUM_ORDER_TYPE_MARKET {
   OpenBuy         = OP_BUY,
   OpenSell        = OP_SELL,
};
