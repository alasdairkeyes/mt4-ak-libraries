//+------------------------------------------------------------------+
//|                                     Enumerators/Logging/Full.mqh |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| See LICENSE file for License                                     |
//| See README.md file for usage                                     |
//|                                                                  |
//| Provide Constants and Enuerators for Syslog style logging        |
//+------------------------------------------------------------------+

// Define Log Constants
#define LOG_OFF       0
#define LOG_DEBUG     10
#define LOG_INFO      20
#define LOG_NOTICE    30
#define LOG_WARNING   40
#define LOG_ERROR     50
#define LOG_CRITICAL  60
#define LOG_ALERT     70
#define LOG_EMERGENCY 80

// This enum maps the standardish Syslog Log levels
enum ENUM_LOGLEVELS {
   Off         = LOG_OFF,
   Debug       = LOG_DEBUG,
   Info        = LOG_INFO,
   Notice      = LOG_NOTICE,
   Warning     = LOG_WARNING,
   Error       = LOG_ERROR,
   Critical    = LOG_CRITICAL,
   Alert       = LOG_ALERT,
   Emergency   = LOG_EMERGENCY
};
