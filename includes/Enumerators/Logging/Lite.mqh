//+------------------------------------------------------------------+
//|                                     Enumerators/Logging/Full.mqh |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| See LICENSE file for License                                     |
//| See README.md file for usage                                     |
//|                                                                  |
//| Provide constants and Enumerators for basic logging              |
//+------------------------------------------------------------------+

// Define Log Constants
#define LOG_OFF       0
#define LOG_BASIC     10
#define LOG_FULL      20

// This enum maps the basic log levels
enum ENUM_LOGLEVELS {
   Off   = LOG_OFF,
   Basic = LOG_BASIC,
   Full  = LOG_FULL,
};
