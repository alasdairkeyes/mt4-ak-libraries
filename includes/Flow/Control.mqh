//+------------------------------------------------------------------+
//|                                                 Flow/Control.mqh |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| See LICENSE file for License                                     |
//| See README.md file for usage                                     |
//|                                                                  |
//| Functions to aid with EA Flow                                    |
//+------------------------------------------------------------------+

/*
   Function to detect if execution has already occured for this current bar.
*/

datetime RunThisBarTime;

bool RunThisBar()
{
   if (RunThisBarTime && Time[0] == RunThisBarTime) {
      return true;
   }

   RunThisBarTime = Time[0];
   return false;
}
