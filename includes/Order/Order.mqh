//+------------------------------------------------------------------+
//|                                                  Order/Order.mqh |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| See LICENSE file for License                                     |
//| See README.md file for usage                                     |
//|                                                                  |
//| Functions to aid with order sizing and placement                 |
//+------------------------------------------------------------------+

/*
   Return a lot size based on a given risk and your current account balance
*/
double LotSizeFromBalance (double LotsWanted, double ForUnitBalanceCurrency, string Symbol = "")
{
   // Auto-set symbol to current symbol if not provided
   if (Symbol == "")
      Symbol = Symbol();

   // Get the minimum Lot Size for this symbol
   double MinimumLotSize = MarketInfo(Symbol, MODE_MINLOT);

   // Calculate desired lot size
   double Lot = NormalizeDouble(AccountBalance() / ForUnitBalanceCurrency * LotsWanted, 2);

   // Check if it's too small
   if (Lot < MinimumLotSize) {
      return NULL;
   } else {
      return Lot;
   }
}

/*
   Return the number of orders of a specific Order type for the given MagicNumber and Symbol
*/
int NumberOfOrdersOfType(ENUM_ORDER_TYPE_ALL OrderType, int MagicMA, string Symbol = "")
{
   // Auto-set symbol to current symbol if not provided
   if (Symbol == "")
      Symbol = Symbol();

   int OrderCount = 0;
   for(int i = 0; i < OrdersTotal(); i++) {
      if(OrderSelect(i, SELECT_BY_POS, MODE_TRADES) == false)
         break;
      if(OrderSymbol() == Symbol && OrderMagicNumber() == MagicMA && OrderType() == OrderType)
         OrderCount++;
   }
   return OrderCount;
}

/*
   Close all pending orders by type, MagicNumber and symbol
   Returns the number of orders succesfully closed
*/
int CloseAllPendingOrdersOfType(ENUM_ORDER_TYPE_PENDING CloseOrderType, int MagicMA, string Symbol = "", color CloseColor = clrWhite)
{
   // Auto-set symbol to current symbol if not provided
   if (Symbol == "")
      Symbol = Symbol();

   int CloseCount = 0;

   // While loop used as for loop gets messed up when OrderDelete() is used
   int OrderIndex = 0;
   while (OrderIndex < OrdersTotal()) {
     if(OrderSelect(OrderIndex, SELECT_BY_POS, MODE_TRADES) == false)
        break;

     if(OrderMagicNumber() == MagicMA && OrderSymbol() == Symbol && OrderType() == CloseOrderType) {
        int OrderTicket = OrderTicket();
        Print("Closing ", OrderTicket());
        int DelRes = OrderDelete(OrderTicket, CloseColor);
        if (!DelRes) {
           PrintFormat(
              "Error closing %d order %d: %d",
              CloseOrderType,
              OrderTicket,
              GetLastError()
           );
        } else {
           PrintFormat("Delete Order %d OK", OrderTicket);
           CloseCount++;
           // When order is succesfully deleted, the orders array elements shuffle
           // up one, continue here, don't increment OrderIndex
           continue;
        }
     }

     OrderIndex++;
   }
   return CloseCount;
}

/*
   Close all Pending orders with given Magic Number and Symbol
   Returns the number of orders succesfully closed
*/
int CloseAllPendingOrders(int MagicMA, string Symbol = "", color CloseColor = clrWhite)
{
   // Auto-set symbol to current symbol if not provided
   if (Symbol == "")
      Symbol = Symbol();

   int CloseCount = 0;
   CloseCount += CloseAllPendingOrdersOfType(OP_SELLSTOP, MagicMA, Symbol, CloseColor);
   CloseCount += CloseAllPendingOrdersOfType(OP_SELLLIMIT, MagicMA, Symbol, CloseColor);
   CloseCount += CloseAllPendingOrdersOfType(OP_BUYSTOP, MagicMA, Symbol, CloseColor);
   CloseCount += CloseAllPendingOrdersOfType(OP_BUYLIMIT, MagicMA, Symbol, CloseColor);

   return CloseCount;
}

/*
   Close all Open orders with given Magic Number and Symbol
   Returns the number of orders succesfully closed
*/
int CloseAllOpenOrders(int MagicMA, string Symbol = "", int Slippage = 3, color CloseColor = clrWhite)
{
   int CloseCount = 0;
   CloseCount += CloseAllOpenOrdersOfType(OP_SELL, MagicMA, Symbol, Slippage, CloseColor);
   CloseCount += CloseAllOpenOrdersOfType(OP_BUY, MagicMA, Symbol, Slippage, CloseColor);

   return CloseCount;
}

/*
   Close all open orders by type, MagicNumber and symbol
   Returns the number of orders succesfully closed
*/
int CloseAllOpenOrdersOfType(ENUM_ORDER_TYPE_MARKET OrderType, int MagicMA, string Symbol = "", int Slippage = 3, color CloseColor = clrWhite)
{
   // Auto-set symbol to current symbol if not provided
   if (Symbol == "")
      Symbol = Symbol();

   int CloseCount = 0;

   for(int i = 0; i < OrdersTotal(); i++) {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) == false)
         break;

      if(OrderSymbol() == Symbol && OrderMagicNumber() == MagicMA && OrderType() == OrderType) {
         double ClosePrice = Bid;
         if (OrderType == OP_SELL)
            ClosePrice = Ask;
         // Close
         int res = OrderClose(
            OrderTicket(),
            OrderLots(),
            ClosePrice,
            Slippage,
            CloseColor
         );
         if (res > 0) {
            Print("Failed to Close Order %d: %d", OrderTicket(), res);
         } else {
            Print("Closed Order %d OK", OrderTicket());
            CloseCount++;
         }
      }

   }

   return CloseCount;
}
