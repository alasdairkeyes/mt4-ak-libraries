//+------------------------------------------------------------------+
//|                                                  Error/Error.mqh |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| See LICENSE file for License                                     |
//| See README.md file for usage                                     |
//|                                                                  |
//| Functions to aid with EA Errors                                  |
//+------------------------------------------------------------------+

/*
   Return the error code and message of the last stored Error.
   Similar to the built-in GetLastError() except that it queries the error
   information to display the error message.

   This requires that your script import the MQL4 standard error headers
   #include <stderror.mqh>
   #include <stdlib.mqh>
*/
string GetLastErrorString(bool ResetErrorCode = true)
{
   // Read _LastError, don't use GetLastError() as that resets the code
   int ErrorCode = _LastError;
   if (ErrorCode == 0)
      return "";

   // Reset if we need to
   if (ResetErrorCode)
      ResetLastError();

   return StringFormat(
      "Error - Code:%d Message:%s",
      ErrorCode,
      ErrorDescription(ErrorCode)
   );
}
