//+------------------------------------------------------------------+
//|                                                  Logging/Log.mqh |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| See LICENSE file for License                                     |
//| See README.md file for usage                                     |
//|                                                                  |
//| Provide logging functions                                        |
//+------------------------------------------------------------------+

/*
   Print output to Console based on provided debug level
*/
void Log(int MessageLevel, string LogMessage) {
   if (LogLevel >= MessageLevel)
      Print(LogMessage);
}
