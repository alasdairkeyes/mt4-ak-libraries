//+------------------------------------------------------------------+
//|                                                         Lite.mq4 |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| Example of using Lite Logging                                    |
//+------------------------------------------------------------------+
#property copyright "Alasdair Keyes"
#property link      "https://gitlab.com/alasdairkeyes/mt4-ak-libraries"
#property version   "1.00"
#property strict

#include "../../includes/Enumerators/Logging/Full.mqh"
#include "../../includes/Logging/Log.mqh"

// Define the LogLevels via the EA configurator, defaulting to OFF
input ENUM_LOGLEVELS  LogLevel = LOG_OFF

int OnInit()
  {
   return(INIT_SUCCEEDED);
  }

void OnDeinit(const int reason)
  {
  }

void OnTick()
  {
      Log(LOG_DEBUG,"Debug");
      Log(LOG_INFO,"Info");
      Log(LOG_NOTICE,"Notice");
      Log(LOG_WARNING,"Warning");
      Log(LOG_ERROR,"Error");
      Log(LOG_CRITICAL,"Critical");
      Log(LOG_ALERT,"Alert");
      Log(LOG_EMERGENCY,"Emergency");
  }
