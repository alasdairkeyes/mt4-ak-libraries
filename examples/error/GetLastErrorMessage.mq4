//+------------------------------------------------------------------+
//|                                                 GetLastError.mq4 |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| Example of the GetLastErrorString() function                     |
//+------------------------------------------------------------------+
#property copyright "Alasdair Keyes"
#property link      "https://gitlab.com/alasdairkeyes/mt4-ak-libraries"
#property version   "1.00"
#property strict

#include <stderror.mqh>
#include <stdlib.mqh>
#include "../../includes/Error/Error.mqh"

int OnInit()
{
   return(INIT_SUCCEEDED);
}

void OnDeinit(const int reason)
{
}

void OnTick()
{
   int ResultA = OrderSend("EURUSD", OP_BUY, 0.01, 20000, 3, 21000, 19000, "Illegal Order", 0, 0, Blue);
   if (ResultA < 0) {
      // This defaults to resetting _LastError
      Alert(GetLastErrorString());
      Alert(StringFormat("_LastError is reset to %d", _LastError));
   }

   int ResultB = OrderSend("EURUSD", OP_BUY, 0.01, 20000, 3, 21000, 19000, "Illegal Order", 0, 0, Blue);
   if (ResultB < 0) {
      // This preserves _LastError for future use.
      Alert(GetLastErrorString(false));
      Alert(StringFormat("_LastError is preserved with %d", _LastError));
   }
}
