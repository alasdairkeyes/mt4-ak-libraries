//+------------------------------------------------------------------+
//|                                           LotSizeFromBalance.mq4 |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| Example for the LotSizeFromBalance() function                    |
//+------------------------------------------------------------------+
#property copyright "Alasdair Keyes"
#property link      "https://gitlab.com/alasdairkeyes/mt4-ak-libraries"
#property version   "1.00"
#property strict

#include "../../includes/Order/Order.mqh"

int OnInit()
  {
   return(INIT_SUCCEEDED);
  }

void OnDeinit(const int reason)
  {
  }

void OnTick()
  {
     double LotSize = LotSizeFromBalance(0.1, 10000)
     if (LotSize == NULL) {
        Print("Balance too low for desired lot size");
        return;
     }
     int Result = OrderSend(
        Symbol(),
        OP_BUY,
        LotSize,
        Ask,
        3,
        0,
        0,
        "OrderComment",
        8235835,
        0,
        Blue
     );
  }
