//+------------------------------------------------------------------+
//|                                         NumberOfOrdersOfType.mq4 |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| Example for the NumberOfOrdersOfType() function                  |
//+------------------------------------------------------------------+
#property copyright "Alasdair Keyes"
#property link      "https://gitlab.com/alasdairkeyes/mt4-ak-libraries"
#property version   "1.00"
#property strict
#define MAGICMA 6498746

#include "../Include/includes/Enumerators/Order/Order.mqh";
#include "../../includes/Order/Order.mqh"

int OnInit()
  {
   return(INIT_SUCCEEDED);
  }

void OnDeinit(const int reason)
  {
  }

void OnTick()
  {
     int BuyOrdersCurrentSymbol = NumberOfOrdersOfType(OP_BUY, MAGICMA);
     int BuyOrdersGBPUSD = NumberOfOrdersOfType(OP_BUY, MAGICMA, "GBPUSD");

     PrintFormat(
       "You have %d open orders of %s",
       BuyOrdersCurrentSymbol,
       Symbol()
     );
     PrintFormat(
       "You have %d open orders of %s",
       BuyOrdersGBPUSD,
       "GBPUSD"
     );
  }
