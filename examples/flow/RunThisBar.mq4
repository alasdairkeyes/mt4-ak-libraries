//+------------------------------------------------------------------+
//|                                                   RunThisBar.mq4 |
//|                                                   Alasdair Keyes |
//|                https://gitlab.com/alasdairkeyes/mt4-ak-libraries |
//|                                                                  |
//| Example of the RunThisBar() function                             |
//+------------------------------------------------------------------+
#property copyright "Alasdair Keyes"
#property link      "https://gitlab.com/alasdairkeyes/mt4-ak-libraries"
#property version   "1.00"
#property strict

#include "../../includes/Flow/Control.mqh"

int OnInit()
  {
   return(INIT_SUCCEEDED);
  }

void OnDeinit(const int reason)
  {
  }

void OnTick()
  {
      if (RunThisBar() == true)
         return;

      // Your code here
  }
