# mt4-ak-libraries


**Note: This library is still in alpha, breaking changes between releases should be expected**

Collection of Libraries to extend MQL4 functionality

## Log

These allow you do provide Output to the Experts log with varying degrees of importance

### Full Logging

Add logging based on the Syslog levels of `LOG`, `INFO`, `NOTICE`, `WARNING`, `ERROR`, `CRITICAL`, `ALERT`, `EMERGENCY`.

#### Import

Import this header with the following Line.

```
  // Load constants/enums
#include "path/to/Enumerators/Logging/Full.mqh"
  // Load Log() Functions
#include "path/to/Logging/Log.mqh"

  // Set the LogLevel via the EA Configure window
input ENUM_LOGLEVELS  LogLevel = LOG_OFF;
  // or Set the LogLevel directly via code
ENUM_LOGLEVELS  LogLevel = LOG_OFF;
```

This imports the following into your script's namespace

#### Constants

* `LOG_OFF`
* `LOG_LOG`
* `LOG_INFO`
* `LOG_NOTICE`
* `LOG_WARNING`
* `LOG_ERROR`
* `LOG_CRITICAL`
* `LOG_ALERT`
* `LOG_EMERGENCY`

#### Enumerators

* `ENUM_LOGLEVELS`

#### EA Input variables

* `LogLevel` - Inserts enum drop-box to select the log level. Default is `LOG_OFF`

#### Functions

```
Log(
    int     MessageLevel,   // The Log level of this message
    string  LogMessage      // The message to output
);
```

#### Example

```
Log(
  LOG_INFO,
  "This is a LOG_INFO warning, it will display if LogLevel is set to LOG_INFO or higher"
);

int SomeInt = 3;
Log(
  LOG_CRITICAL,
  StringFormat(
    "This is a LOG_CRITICAL message to report the value of SomeInt=%d", SomeInt)
);
```

### Lite Logging

Add logging based on the levels of `BASIC`, `FULL`.

#### Import

Import this header with the following Line.

```
// Load constants/enums
#include "path/to/Enumerators/Logging/Lite.mqh"
// Load Log() Functions
#include "path/to/Logging/Log.mqh"

// Set the LogLevel via the EA Configure window
input ENUM_LOGLEVELS  LogLevel = LOG_OFF;
// or Set the LogLevel directly via code
ENUM_LOGLEVELS  LogLevel = LOG_OFF;
```

This imports the following into your script's namespace

#### Constants

* `LOG_OFF`
* `LOG_BASIC`
* `LOG_FULL`

#### Enumerators

* `ENUM_LOGLEVELS`

#### EA Input variables

* `LogLevel` - Inserts enum drop-box to select the log level. Default is `LOG_OFF`

#### Functions

```
Log(
    int     MessageLevel,   // The Log level of this message
    string  LogMessage      // The message to output
);
```

#### Example

```
Log(
  LOG_BASIC,
  "This is an LOG_BASIC warning, it will display if LogLevel is set to LOG_BASIC or higher"
);

int SomeInt = 3;
Log(
  LOG_FULL,
  StringFormat(
    "This is a LOG_FULL message to report the value of SomeInt=%d", SomeInt)
);
```

#### Customisation

You can use your own log levels by creating your own `.mqh` file copied from `Enumerators/Logging/*.mqh` and customise it and include it in place of the standard enumerator files.

## Order

### Lot Size From Balance

This function will return a suitable lot size based on your required entry ratio against your current balance.

You may wish that for every 10000 units of currency in your account balance, you wish to place a lot size of 0.1

If your account balance is 5000 units, this will return 0.05
If your account balance is 20000 units, this will return 0.2
If your account balance is low, and you can't match the minimum lot size for your currency, this will return NULL

#### Import

```
#include "path/to/Order/Order.mqh"
```

#### Functions

```
double LotSizeFromBalance(
    double  LotsWanted,               // The number of lots required
    double  ForUnitBalanceCurrency,   // Against this many units of currency in your account balance
    string  Symbol                    // Optional Symbol, this will default to your current symbol
);
```

#### Example

```
double LotSize = LotSizeFromBalance(10000, 0.1);

double EURUSDLotSize = LotSizeFromBalance(10000, 0.1, "EURUSD");

```

### Number Of Orders Of Type

This function returns the number of open orders you have for a given currency, magic number.

Setting MAGICMA to 0 will close orders created manually.

#### Import

```
#include "path/to/Enumerators/Order/Order.mqh"
#include "path/to/Order/Order.mqh"
```

#### Functions

```
int NumberOfOrdersOfType(
    int OrderType,              // The Order Type you wish to count
    int MagicMA,                // The Magic number associated with the trades
    string Symbol = ""          // Optional Symbol, this will default to your current symbol
);
```

#### Example

```
int BuyOrdersCurrentSymbol = NumberOfOrdersOfType(OP_BUY, MAGICMA);
int BuyOrdersGBPUSD = NumberOfOrdersOfType(OP_BUY, MAGICMA, "GBPUSD");
```

### Close Pending Orders Of Type

This closes all pending orders of a particular type for the given symbol and Magic Number.

Returns the number of orders closed.

Setting MAGICMA to 0 will close orders created manually.

#### Import

```
#include "path/to/Enumerators/Order/Order.mqh"
#include "path/to/Order/Order.mqh"
```

#### Functions

```
int CloseAllPendingOrdersOfType(
    int CloseOrderType,         // The Pending Order Type you wish to close
    int MagicMA,                // The Magic number associated with the trades
    string Symbol = "",         // Optional Symbol, this will default to your current symbol
    color CloseColor = clrWhite // Optional color arrow to draw on the chart, defaults to white
);
```

#### Example

```
int CloseCurrentSymbolBuyStops = CloseAllPendingOrdersOfType(OP_BUYSTOP, MAGICMA);
int CloseGBPUSDBuyLimits = CloseAllPendingOrdersOfType(OP_BUYLIMIT, MAGICMA, "GBPUSD");
int CloseUSDCADSellLimits = CloseAllPendingOrdersOfType(OP_SELLLIMIT, MAGICMA, "USDCAD", clrGreen);
```

### Close All Pending Orders

This closes all pending orders for the given symbol and Magic Number.

Returns the number of orders closed.

Setting MAGICMA to 0 will close orders created manually.

#### Import

```
#include "path/to/Enumerators/Order/Order.mqh"
#include "path/to/Order/Order.mqh"
```

#### Functions

```
CloseAllPendingOrders(
    int MagicMA,                // The Magic number associated with the trades
    string Symbol = "",         // Optional Symbol, this will default to your current symbol
    color CloseColor = clrWhite // Optional color arrow to draw on the chart, defaults to white
);

```

#### Example

```
int CloseCurrentSymbolPending = CloseAllPendingOrders(MAGICMA);
int CloseGBPUSDPending = CloseAllPendingOrders(MAGICMA, "GBPUSD");
int CloseUSDCADPending = CloseAllPendingOrders(MAGICMA, "USDCAD", clrGreen);
int CloseAllNonEAOrdersPending = CloseAllPendingOrders(0);
```

### Close Open Orders Of Type

This closes all open orders of a particular type for the given symbol and Magic Number.

Returns the number of orders closed.

Setting MAGICMA to 0 will close orders created manually.

#### Import

```
#include "path/to/Enumerators/Order/Order.mqh"
#include "path/to/Order/Order.mqh"
```

#### Functions

```
int CloseAllOpenOrdersOfType(
    int OrderType,              // The Order Type you wish to close
    int MagicMA,                // The Magic number associated with the trades
    string Symbol = "",         // Optional Symbol, this will default to your current symbol
    int Slippage = 3,           // Optional allowed slippage, defaults to 3
    color CloseColor = clrWhite // Optional color arrow to draw on the chart, defaults to white
);
```

#### Example

```
int CloseOpenCurrentSymbolLongs = CloseAllOpenOrdersOfType(OP_BUY, MAGICMA);
int CloseGBPUSDLongs = CloseAllOpenOrdersOfType(OP_BUY, MAGICMA, "GBPUSD");
int CloseUSDCADLongs = CloseAllOpenOrdersOfType(OP_BUY, MAGICMA, "USDCAD", 20, clrRed);
```

### Close All Open Orders

This closes all open orders for the given symbol and Magic Number.

Returns the number of orders closed.

Setting MAGICMA to 0 will close orders created manually.

#### Import

```
#include "path/to/Enumerators/Order/Order.mqh"
#include "path/to/Order/Order.mqh"
```

#### Functions

```
CloseAllOpenOrders(
    int MagicMA,                // The Magic number associated with the trades
    string Symbol = "",         // Optional Symbol, this will default to your current symbol
    int Slippage = 3,           // Optional allowed slippage, defaults to 3
    color CloseColor = clrWhite // Optional color arrow to draw on the chart, defaults to white
);

```

#### Example

```
int CloseAllCurrentSymbolOpen = CloseAllOpenOrders(MAGICMA);
int CloseAllGBPUSDOpen = CloseAllOpenOrders(MAGICMA, "GBPUSD");
int CloseAllUSDCADOpen = CloseAllOpenOrders(MAGICMA, "USDCAD", 20, clrRed);
int CloseAllNonEAGBPUSDOrdersOpen = CloseAllOpenOrders(0, "GBPUSD");
```

## Flow

### RunThisBar()

This function will store the last time your code in `OnTick()` was executed.
If a new bar has opened and this is the first tick being run on the new bar, it will return `false`
If your code has already executed on this bar, it will return `true`.

#### Import

```
#include "path/to/Flow/Control.mqh"
```

#### Functions

```
bool RunThisBar();
```

#### Example

```
if (RunThisBar() == true)
  return;
```
